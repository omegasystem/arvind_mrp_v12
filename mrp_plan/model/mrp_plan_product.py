# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _


class MrpPlanProduct(models.Model):
    _name = 'mrp.plan.product'
    _description = 'Mrp Plan Product'

    name = fields.Char('Name', readonly=False)
    mrp_plan_id = fields.Many2one('mrp.plan', string='MRP Plan')
    sale_order_id = fields.Many2one('sale.order', string='Sale order')
    shipment_date = fields.Date('Shipment Date')
    quantity = fields.Integer('Quantity')
    product_id = fields.Many2one('product.product', 'Product', domain="[('bom_ids','!=',False)]")
    status = fields.Selection([
        ('created', 'Created'),
        ('in_progress', 'In Progress'),
        ('plan_done', 'Done')], string='Status', default='created')

    def unlink_product(self):
        if self.env.context.get('active_ids'):
            active_ids = self.browse(self.env.context.get('active_ids'))
            for record in active_ids:
                record.sale_order_id = False


class ProductProduct(models.Model):
    _inherit = 'product.product'

    default_curing_day = fields.Integer('Curing Day')
    labour_cost = fields.Float('Labour Cost(Per 1)')
