# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    pan = fields.Char('PAN NO')


class ResCompany(models.Model):
    _inherit = 'res.company'

    pan = fields.Char('PAN NO')
