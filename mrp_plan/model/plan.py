from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError


class MrpBom(models.Model):
    _inherit = 'mrp.bom'

    is_subcontract = fields.Boolean('Subcontract', default=False)


class Planning(models.Model):
    _name = 'mrp.plan'
    _description = 'MRP Planning'

    def change_color_on_kanban(self):
        for record in self:
            color = 0
            if record.state == 'draft':
                color = 4
            elif record.state == 'approve':
                color = 10
            elif record.state == 'cancel':
                color = 9
            record.color = color

    name = fields.Char('Name', readonly=True)
    #     location_dest_id = fields.Many2one('stock.location','Destination Loaction')
    product_id = fields.Many2one('product.product', 'Product', domain="[('bom_ids','!=',False)]")
    plan_date = fields.Datetime('Plan Date', default=datetime.today())
    production_date = fields.Datetime('Production Date')
    plan_qty = fields.Float('Plan Quantity')
    uom_id = fields.Many2one('uom.uom', 'Product Uom')
    bom_id = fields.Many2one('mrp.bom', 'Bom', domain="[('product_tmpl_id','=',product_id)]")
    user_id = fields.Many2one('res.users', 'Responsible')
    is_subcontract = fields.Boolean('Subcontract', default=False, compute="onchange_subcontract")
    state = fields.Selection([('draft', 'Draft'), ('approve', 'Approve'), ('cancel', 'Cancel'), ('done', 'Done')], copy=False,
                             default="draft")
    production_id = fields.Many2one('mrp.production', 'Production', readonly=True)
    color = fields.Integer('Color Index', compute="change_color_on_kanban")
    progress = fields.Integer('Production Progress', default=10, compute="onchange_widget")
    max_progress = fields.Integer(default=100)
    curing_status = fields.Selection([
        ('none', 'None'),
        ('curing', 'Send to Curing'),
        ('curing_process', 'Curing Process'),
        ('curing_done', 'Curing Complete'),
    ], string='Curing Status', default='none')
    current_status = fields.Selection([
        ('none', 'None'),
        ('send', 'Send to Store Manager'),
        ('complete', 'Complete')
    ], string='Current Status',  default='none')
    curing_date = fields.Date('Curing Start Date')
    curing_end_date = fields.Date('Curing End Date')
    curing_days = fields.Integer('Curing Day')
    remain_curing_days = fields.Integer('Remain Curing Day', compute="get_remain_curing_days")
    mrp_plan_product_id = fields.Many2one('mrp.plan.product', 'MRP Plan Product', readonly=True)
    force_curing = fields.Boolean('Force Curing', default=False)

    @api.depends('curing_days', 'curing_date')
    def get_remain_curing_days(self):
        for record in self:
            print("record-----------------------------")
            # if record.force_curing is True and record.curing_status == 'curing_process':
            #     record.remain_curing_days = 0
            #     record.write({'curing_status': 'curing_done'})
            #     for num in range(0, int(record.plan_qty)):
            #         product_number = self.env['ir.sequence'].next_by_code('mrp.plan.product')
            #         plan_product_id = self.env['mrp.plan.product'].sudo().create({
            #             'name': record.name + "-" + product_number,
            #             'mrp_plan_id': record.id,
            #             'shipment_date': False,
            #             'product_id': record.product_id.id,
            #             'quantity': 1
            #         })
            #         record.write({'mrp_plan_product_id': plan_product_id.id})

            if record.curing_date and record.force_curing is False and record.curing_status == 'curing_process':
                # today_date = fields.Date.today() + timedelta(days=7)
                if record.curing_date > fields.Date.today():
                    record.remain_curing_days = record.curing_days
                    record.write({'curing_status': 'curing'})
                if record.curing_date <= fields.Date.today() and record.curing_end_date >= fields.Date.today():
                    print("second------------------------")
                    days_gone = fields.Date.today() - record.curing_date
                    record.remain_curing_days = record.curing_days - days_gone.days
                    record.write({'curing_status': 'curing_process'})
                    if record.remain_curing_days == 0:
                        record.write({'curing_status': 'curing_done'})
                        if not record.mrp_plan_product_id:
                            for num in range(0, int(record.plan_qty)):
                                product_number = self.env['ir.sequence'].next_by_code('mrp.plan.product')
                                plan_product_id = self.env['mrp.plan.product'].sudo().create({
                                    'name': record.name + "-" + product_number,
                                    'mrp_plan_id': record.id,
                                    'shipment_date': False,
                                    'product_id': record.product_id.id,
                                    'quantity': 1
                                })
                                record.write({'mrp_plan_product_id': plan_product_id.id})

    @api.multi
    def view_mrp_plan_product(self):
        action = self.env.ref('mrp_plan.action_mrp_plan_product').read()[0]
        action['domain'] = [('mrp_plan_id', '=', self.id)]
        action['views'] = [(self.env.ref('mrp_plan.view_mrp_plan_product_tree').id, 'tree')]
        return action

    @api.depends('production_id')
    def onchange_widget(self):
        for rec in self:
            if rec.production_id:
                if rec.production_id.state == 'confirmed':
                    rec.progress = 25
                elif rec.production_id.state == 'planned':
                    rec.progress = 50
                elif rec.production_id.state == 'progress':
                    rec.progress = 75
                elif rec.production_id.state == 'done':
                    rec.progress = 100
                    rec.write({'state': 'done'})
                elif rec.production_id.state == 'cancel':
                    rec.progress = 0

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('mrp.plan') or _('New')
        return super(Planning, self).create(vals)

    @api.onchange('product_id')
    def onchange_bom(self):
        if self.product_id:
            print(self.product_id)
            product_id = self.env['product.template'].search([('name', '=', self.product_id.name)], limit=1)
            print(product_id)
            bom = self.env['mrp.bom'].search([('product_tmpl_id', '=', product_id.id)], limit=1)
            self.bom_id = bom
            self.uom_id = self.product_id.uom_id.id
            self.plan_qty = 1

    @api.onchange('production_date')
    def onchange_date(self):
        if self.production_date and self.plan_date:
            if self.production_date < self.plan_date:
                raise ValidationError('Production Date should be greater than Plan Date...!!')

    @api.depends('bom_id')
    def onchange_subcontract(self):
        for rec in self:
            if rec.bom_id:
                if rec.bom_id.is_subcontract:
                    rec.is_subcontract = rec.bom_id.is_subcontract

    #     def confirm(self):
    #         if self.state:
    #             self.ensure_one()
    #             template = self.env['ir.model.data'].get_object('mrp_plan', 'email_template_mrp_plan')
    #             self.env['mail.template'].browse(template.id).send_mail(self.id,force_send=True)
    #             self.state = 'confirm'

    def cancel(self):
        if self.state:
            self.state = 'cancel'

    def start_curing(self):
        if self.product_id:
            if self.product_id.default_curing_day <= 0:
                raise ValidationError('Curing day not configured in product!!')
            self.curing_date = fields.Date.today()
            self.curing_status = 'curing_process'
            self.curing_days = self.product_id.default_curing_day
            self.curing_end_date = self.curing_date + timedelta(days=self.product_id.default_curing_day)

    def force_curing_done(self):
        self.curing_status = 'curing_done'
        self.remain_curing_days = 0
        self.force_curing = True
        for num in range(0, int(self.plan_qty)):
            product_number = self.env['ir.sequence'].next_by_code('mrp.plan.product')
            plan_product_id = self.env['mrp.plan.product'].sudo().create({
                'name': self.name + "-" + product_number,
                'mrp_plan_id': self.id,
                'shipment_date': False,
                'product_id': self.product_id.id,
                'quantity': 1
            })
            self.write({'mrp_plan_product_id': plan_product_id.id})

    def complete(self):
        self.current_status = 'complete'

    def approve(self):
        if self.product_id:
            picking_type_id = self.env['stock.picking.type'].search(
                [('code', '=', 'mrp_operation'), ('warehouse_id.partner_id', '=', self.env.user.company_id.id)])
            mrp_order = {
                'product_id': self.product_id.id,
                'planned_date': self.plan_date,
                'product_qty': self.plan_qty,
                'product_uom_id': self.uom_id.id,
                'date_planned_start': self.production_date,
                'bom_id': self.bom_id.id,
                'user_id': self.user_id.id,
                #                     'location_dest_id':self.location_dest_id.id,
                'is_subcontract': self.is_subcontract,
                # 'partner_id':self.bom_id.partner_id.id,
                'picking_type_id': picking_type_id.id,
                'location_src_id': picking_type_id.default_location_src_id.id,
                'planned_id': self.id or '',
            }
            mrp = self.env['mrp.production'].create(mrp_order)
            self.production_id = mrp
            template = self.env['ir.model.data'].get_object('mrp_plan', 'email_template_mrp_plan')
            self.env['mail.template'].browse(template.id).send_mail(self.id, force_send=True)
            self.state = 'approve'

    def manufacture_order(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'mrp.production',
            'res_id': self.production_id.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'current',
        }

    # @api.multi
    # def view_contractor_billing(self):
    #     action = self.env.ref('account.action_invoice_tree').read()[0]
    #     action['domain'] = [('mrp_plan_id', '=', self.id)]
    #     action['views'] = [(self.env.ref('account.invoice_tree').id, 'tree')]
    #     return action

    def view_contractor_billing(self):
        ctx = {}
        return {
            'name': _('Account Invoice'),
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'res_model': 'account.invoice',
            'target': 'current',
            'view_id': self.env.ref('account.invoice_tree').id,
            'context': {
                'default_mrp_plan_id': self.id,
            },
            'domain': [('mrp_plan_id', '=', self.id)]
        }


    def send_ready_product(self):
        self.current_status = 'send'

    def send_curing(self):
        if self.state == 'done':
            self.curing_status = 'curing'

    @api.multi
    def write(self, vals):
        return super(Planning, self).write(vals)

    @api.multi
    def generate_billing(self):
        product_id = self.env.ref('mrp_plan.labour_cost_product')
        sale_order_obj = self.env['sale.order']
        partner_id = self.env['res.partner'].search([
            ('email', '=', self.user_id.login)
        ])
        so_id = sale_order_obj.create({
            'partner_id': partner_id.id,
            'partner_invoice_id': partner_id.id,
            'partner_shipping_id': partner_id.id,
        })
        self.env['sale.order.line'].create({
            'name': product_id.name,
            'product_id': product_id.id,
            'product_uom_qty': self.plan_qty,
            'price_unit': product_id.labour_cost,
            'order_id': so_id.id,
            'company_id': so_id.company_id.id,
        })
        so_id.action_confirm()
        so_id.action_invoice_create()


class Production(models.Model):
    _inherit = 'mrp.production'

    planned_date = fields.Char('Planned Date')
    planned_id = fields.Many2one('mrp.plan', 'Production Plan')
    supplier_id = fields.Many2one('res.partner', string='Supplier')
    carrier_id = fields.Many2one('delivery.carrier', string='Carrier')
    vehicle_number = fields.Char('Vehicle Number')
    shipment_number = fields.Char('Shipment Number')
    shipment_date = fields.Date('Shipment Date')


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    mrp_plan_id = fields.Many2one('mrp.plan', string='Plan ID')
    eway_details = fields.Text('Eway Bill Details')
    Vehicle_num = fields.Char('Vehicle Number')

    # @api.model
    # def create(self, vals):
    #     if self.env.context.get('active_model') == 'mrp.plan':
    #         vals.update({'mrp_plan_id': self.env.context.get('active_id')})
    #     return super(AccountInvoice, self).create(vals)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    mrp_plan_id = fields.Many2one('mrp.plan', string='Plan ID')

    @api.model
    def create(self, vals):
        print("create---------------------", self.env.context)
        if 'params' in self.env.context:
            if self.env.context['params'].get('model') == 'mrp.plan':
                active_id = self.env.context['params'].get('id')
                vals.update({'mrp_plan_id': active_id})
        return super(SaleOrder, self).create(vals)

    @api.multi
    def _prepare_invoice(self):
        print("_prepare_invoice----------------------")
        invoice_vals = super(SaleOrder, self)._prepare_invoice()
        invoice_vals.update({
            'mrp_plan_id': self.mrp_plan_id.id,
        })
        return invoice_vals
