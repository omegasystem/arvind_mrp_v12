# -*- coding: utf-8 -*-
{
    "name": "Mapol MRP Planning",
    "summary": "MRP Planning",
    "version": "12.1",
    "category": "Tools",
    'author': 'Mapol Business Solution Pvt Ltd',
    'website': 'http://www.mapolbs.com/',
    "description": """
        This module helps to plan the production.
    """,
    "license": "LGPL-3",
    "installable": True,
    'application': True,
    'auto_install': False,
    'images': ['static/description/icon.png'],
    "depends": [
        'base', 'stock', 'product', 'mrp', 'purchase', 'production_extends', 'purchase_extends'
    ],
    "data": [
        'data/ir_sequence_data.xml',
        'data/product_data.xml',
        'security/ir.model.access.csv',
        'security/mrp_plan_security.xml',
        # 'wizard/curing_wizard_view.xml',
        'data/mail.xml',
        'view/mrp_plan_product_view.xml',
        'view/mrp_plan_view.xml',
        'view/product.xml',
        'view/res_partner_view.xml',
        'reports/report_contractor_payment.xml',
        'reports/report_supplier.xml',
        'reports/report_customer.xml',
        'reports/report_mrp_unit.xml',
        'reports/report_invoice.xml',
        'wizard/contractor_payment_views.xml',
        'wizard/mrp_reports_view.xml',
        'view/menus.xml',
        'reports/invoice_product_qty_view.xml',
        'reports/report_contractor_billing.xml',
        'reports/reports_menu.xml'

    ],
}
