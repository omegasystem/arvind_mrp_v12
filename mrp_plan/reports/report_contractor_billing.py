#-*- coding:utf-8 -*-
from odoo import api, models, _


class ContractorBilling(models.AbstractModel):
    _name = 'report.mrp_plan.report_contractor_billing'

    @api.model
    def _get_report_values(self, docids, data=None):
        invoice_ids = self.env['account.invoice'].browse(data.get('invoice_ids'))
        invoices = []
        for invoice_id in invoice_ids:
            invoices.append(invoice_id)
        return {
            'doc_ids': invoice_ids,
            'doc_model': 'account.invoice',
            'docs': invoices,
            'data': data,
        }
