#-*- coding:utf-8 -*-
from odoo import api, models, _


class ReportMrpUnit(models.AbstractModel):
    _name = 'report.mrp_plan.report_mrp_unit'

    @api.model
    def _get_report_values(self, docids, data=None):
        mrp_plan_ids = self.env['mrp.plan'].browse(data.get('mrp_plan_ids'))
        report_dict = {}
        if data['form'].get('partner_id'):
            partner_id = self.env['res.partner'].browse(data['form'].get('partner_id'))
            report_dict.update({
                'partner_id': partner_id
            })
        mrp_plans_list = []

        for mrp_plan_id in mrp_plan_ids:
            mrp_plan_dict = {}
            mrp_plan_dict.update({'mrp_plan_id': mrp_plan_id})
            mrp_plans_list.append(mrp_plan_dict)
        print("mrp_plans_list--------------", mrp_plans_list)
        report_dict = {
            'doc_ids': docids,
            'doc_model': 'mrp.plan',
            'docs': mrp_plan_ids,
            'data': mrp_plans_list,
        }
        return report_dict
