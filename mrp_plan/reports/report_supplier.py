#-*- coding:utf-8 -*-
from odoo import api, models, _


class ReportSupplier(models.AbstractModel):
    _name = 'report.mrp_plan.report_supplier'

    @api.model
    def _get_report_values(self, docids, data=None):
        purchase_ids = self.env['purchase.order'].browse(data.get('purchase_ids'))
        report_dict = {}
        if data['form'].get('partner_id'):
            partner_id = self.env['res.partner'].browse(data['form'].get('partner_id'))
            report_dict.update({
                'partner_id': partner_id
            })
        purchases_list = []
        for purchase_id in purchase_ids:
            purchases_dict = {}
            purchases_dict.update({'purchase_id': purchase_id})
            purchase_line_list = []
            for line in purchase_id.order_line:
                purchase_line_list.append(line)
            purchases_dict.update({'lines': purchase_line_list})
            purchases_list.append(purchases_dict)
        report_dict = {
            'doc_ids': docids,
            'doc_model': 'purchase.order',
            'docs': data.get('purchase_ids'),
            'data': purchases_list,
        }
        return report_dict
