#-*- coding:utf-8 -*-
from odoo import api, models, _


class ReportCustomer(models.AbstractModel):
    _name = 'report.mrp_plan.report_customer'

    @api.model
    def _get_report_values(self, docids, data=None):
        sales_ids = self.env['sale.order'].browse(data.get('sales_ids'))
        report_dict = {}
        if data['form'].get('partner_id'):
            partner_id = self.env['res.partner'].browse(data['form'].get('partner_id'))
            report_dict.update({
                'partner_id': partner_id
            })
        sales_list = []
        for sale_id in sales_ids:
            sales_dict = {}
            sales_dict.update({'sale_id': sale_id})
            sale_line_list = []
            for line in sale_id.order_line:
                sale_line_list.append(line)
            sales_dict.update({'lines': sale_line_list})
            sales_list.append(sales_dict)
        report_dict = {
            'doc_ids': docids,
            'doc_model': 'sale.order',
            'docs': data.get('sales_ids'),
            'data': sales_list,
        }
        return report_dict
