#-*- coding:utf-8 -*-
from odoo import api, models, _
from datetime import datetime


class WeeklyPipelineReport(models.AbstractModel):
    _name = 'report.mrp_plan.report_contractor_payment'

    @api.model
    def _get_report_values(self, docids, data=None):
        # lead_list = []
        # message_obj = self.env['mail.message']
        # for order in data.get('order_ids'):
        #     lead_data = {}
        #     leads = self.env['crm.lead'].browse(int(order))
        #     message_ids = message_obj.search_count([
        #         ('create_date', '>=', data['form'].get('start_date')),
        #         ('create_date', '<=', data['form'].get('end_date')),
        #         ('res_id', '=', leads.id)
        #     ])
        #     lead_data['leads'] = leads
        #     lead_data['timeline_days'] = (datetime.today() - leads.create_date).days
        #     lead_data['log_count'] = message_ids
        #
        #     so_lines = sorted(leads.order_ids, reverse=True)
        #     if so_lines:
        #         lead_data['so_id'] = so_lines[0]
        #         lead_data['so_name'] = so_lines[0].name
        #         lead_data['so_count'] = len(so_lines)
        #     lead_list.append(lead_data)
        return {
            'doc_ids': data.get('mrp_prod_ids'),
            'doc_model': 'crm.lead',
            'docs': docids,
            'data': data,
        }
