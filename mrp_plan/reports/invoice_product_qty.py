#-*- coding:utf-8 -*-
from odoo import api, models, _
from datetime import datetime


class InvoiceProductQty(models.AbstractModel):
    _name = 'report.mrp_plan.report_invoice_product_qty'

    @api.model
    def _get_report_values(self, docids, data=None):
        products = []
        invoice_id = self.env['account.invoice'].browse(docids)
        if invoice_id.origin:
            so_id = self.env['sale.order'].search([
                ('name', '=', invoice_id.origin)
            ])
            print("so_id----------", so_id)
            if so_id:
                product_ids = self.env['mrp.plan.product'].search([
                    ('sale_order_id', '=', so_id.id)
                ])
                print("product_ids-------", product_ids)
                if product_ids:
                    for product in product_ids:
                        products.append(product)

        print("data-------------", invoice_id, docids)
        return {
            'doc_ids': invoice_id,
            'doc_model': 'account.invoice',
            'docs': products,
            'data': data,
        }
