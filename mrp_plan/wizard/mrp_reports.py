from odoo import api, fields, models, _
import datetime


class MrpReports(models.TransientModel):
    _name = 'mrp.reports'

    report_type = fields.Selection([
        ('supplier', 'Supplier'),
        ('customer', 'Customer'),
        ('contractor', 'Contractor'),
        ('inventory', 'Inventory(Manufacturing Unit)'),
        ('internal', 'Internal Transfers')
    ], string='Report Type', default='supplier')
    billing_status = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('paid', 'Paid')
    ], string='Billing Status', default='draft')
    partner_id = fields.Many2one('res.partner', string="Partner(Supplier/Customer)")
    contract_id = fields.Many2one('res.users', string="Contractor")
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.multi
    def print_report(self):
        mrp_prod_obj = self.env['mrp.production']
        domain = []
        domain += [
            ('create_date', '>=', self.start_date),
            ('create_date', '<=', self.end_date)
        ]
        partner_obj = self.env['res.partner']
        if self.report_type in ['supplier', 'customer']:
            if self.partner_id:
                domain += [('partner_id', '=', self.partner_id.id)]
            report_datas = {}
            if self.report_type in ['supplier']:
                records = self.env['purchase.order'].search(domain)
                [data] = self.read()
                report_datas.update({
                    'ids': [],
                    'model': 'purchase.order',
                    'form': data,
                    'purchase_ids': [i.id for i in records],
                })
                report_name = 'mrp_plan.action_purchase_order_report'
            else:
                records = self.env['sale.order'].search(domain)
                [data] = self.read()
                report_datas.update({
                    'ids': [],
                    'model': 'sale.order',
                    'form': data,
                    'sales_ids': [i.id for i in records],
                })
                report_name = 'mrp_plan.action_sale_order_report'

            return self.env.ref(report_name).report_action(records, data=report_datas)
        elif self.report_type == 'contractor':
            if self.contract_id:
                partner_id = partner_obj.search([
                    ('email', '=', self.contract_id.login)
                ])
                domain += [('partner_id', '=', partner_id.id)]
            if self.billing_status == 'draft':
                domain +=[('state', '=', 'draft')]
            elif self.billing_status == 'open':
                domain += [('state', '=', 'open')]
            else:
                domain += [('state', '=', 'paid')]

            invoice_ids = self.env['account.invoice'].search(domain)
            [data] = self.read()
            datas = {
                'ids': [],
                'model': 'account.invoice',
                'form': data,
                'invoice_ids': [i.id for i in invoice_ids],
            }
            return self.env.ref('mrp_plan.action_contractor_billing_report').report_action(invoice_ids, data=datas)
        elif self.report_type == 'inventory':
            mrp_plan_ids = self.env['mrp.plan'].search(domain)
            print("mrp_plan_ids----------", mrp_plan_ids)
            [data] = self.read()
            datas = {
                'ids': [],
                'model': 'mrp.plan',
                'form': data,
                'mrp_plan_ids': [i.id for i in mrp_plan_ids],
            }
            return self.env.ref('mrp_plan.action_mrp_plan_unit_report').report_action(mrp_plan_ids, data=datas)
        else:
            domain += [('date_planned_start', '>=', self.start_date), ('date_planned_start', '<=', self.end_date)]
            domain += [('user_id', '=', self.contract_id.id)]
            mrp_prod_ids = mrp_prod_obj.search(domain)

            [data] = self.read()
            datas = {
                'ids': [],
                'model': 'mrp.production',
                'form': data,
                'mrp_prod_ids': [i.id for i in mrp_prod_ids],
            }
            return self.env.ref('mrp_plan.action_invoice_product_qty').report_action(mrp_prod_ids, data=datas)
