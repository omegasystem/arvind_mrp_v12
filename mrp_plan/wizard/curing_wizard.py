from odoo import api, fields, models, _
import datetime


class CuringWizard(models.TransientModel):
    _name = 'curing.wizard'

    curing_date = fields.Date('Curing Start Date')
    curing_end_date = fields.Date('Curing End Date')
    curing_days = fields.Integer('Curing Day')

    def update_curing(self):
        mrp_plan_id = self.env['mrp.plan'].browse(self.env.context.get('active_id'))
        mrp_plan_id.write({
            'curing_date': self.curing_date,
            'curing_days': self.curing_days,
            'curing_end_date': self.curing_date + datetime.timedelta(days=self.curing_days)
        })
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }
