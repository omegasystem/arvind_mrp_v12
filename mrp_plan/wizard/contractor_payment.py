from odoo import api, fields, models, _
import datetime


class ContractorPayment(models.TransientModel):
    _name = 'contractor.payment'

    report_type = fields.Selection([
        ('summery', 'Summary of manufactured'),
        ('billing', 'Billing')
    ], string='Report Type', default='summery')
    billing_status = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('paid', 'Paid')
    ], string='Billing Status', default='draft')
    contract_id = fields.Many2one('res.users', string="Contractor")
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.multi
    def print_report(self):
        mrp_prod_obj = self.env['mrp.production']
        domain = []
        if self.report_type == 'billing':
            if self.contract_id:
                partner_id = self.env['res.partner'].search([
                    ('email', '=', self.contract_id.login)
                ])
                domain += [('partner_id', '=', partner_id.id)]
            if self.billing_status == 'draft':
                domain +=[('state', '=', 'draft')]
            elif self.billing_status == 'open':
                domain += [('state', '=', 'open')]
            else:
                domain += [('state', '=', 'paid')]
            domain += [
                ('create_date', '>=', self.start_date),
                ('create_date', '<=', self.end_date)
            ]
            invoice_ids = self.env['account.invoice'].search(domain)
            [data] = self.read()
            datas = {
                'ids': [],
                'model': 'account.invoice',
                'form': data,
                'invoice_ids': [i.id for i in invoice_ids],
            }
            return self.env.ref('mrp_plan.action_contractor_billing_report').report_action(invoice_ids, data=datas)
        else:
            domain += [('date_planned_start', '>=', self.start_date), ('date_planned_start', '<=', self.end_date)]
            domain += [('user_id', '=', self.contract_id.id)]
            mrp_prod_ids = mrp_prod_obj.search(domain)

            [data] = self.read()
            datas = {
                'ids': [],
                'model': 'mrp.production',
                'form': data,
                'mrp_prod_ids': [i.id for i in mrp_prod_ids],
            }
            return self.env.ref('mrp_plan.action_invoice_product_qty').report_action(mrp_prod_ids, data=datas)
