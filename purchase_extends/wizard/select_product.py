# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class WizardSelectProduct(models.TransientModel):
    _name = 'wizard.select.product'

    wizard_product_ids = fields.One2many('wizard.select.product.line', 'wizard_id', string='Products')

    @api.model
    def default_get(self, fields):
        sale_obj = self.env['sale.order']
        res = super(WizardSelectProduct, self).default_get(fields)
        if self.env.context.get('active_id') and self.env.context.get('active_model') == 'sale.order':
            sale_order = sale_obj.browse(self.env.context.get('active_id'))
            line_product_ids = sale_order.order_line.mapped('product_id').ids
            selected_product_ids = self.env['mrp.plan.product'].search(
                [('product_id', 'in', line_product_ids), ('sale_order_id', '=', False)])
            product_ids = [(0, 0,
                            {'product_id': record.product_id.id,
                             'name': record.name,
                             'quantity': record.quantity
                             }) for record in selected_product_ids]
            res.update({'wizard_product_ids': product_ids})
        return res

    @api.multi
    def update_selected_product(self):
        mrp_product = self.env['mrp.plan.product']
        selected_product = self.wizard_product_ids.filtered(lambda x: x.is_select_product is True)
        count_selected = len(selected_product)
        sale_order = self.env['sale.order'].browse(self.env.context.get('active_id'))
        lines = sale_order.order_line.filtered(lambda x: x.is_poll is True)
        count_qty = sum([line.product_uom_qty for line in lines])

        products_count = mrp_product.search_count([
            ('sale_order_id', '=', sale_order.id)
        ])
        if count_selected + products_count != int(count_qty):
            raise ValidationError(_('Kindly select product as per sale order Qty!'))

        list_product_name = selected_product.mapped('name')
        search_mrp_plan_product = mrp_product.search([
            ('name', 'in', list_product_name)
        ])
        search_mrp_plan_product.sudo().write({
            'sale_order_id': sale_order.id,
            'status': 'plan_done'
        })


class WizardProductLines(models.TransientModel):
    _name = 'wizard.select.product.line'

    name = fields.Char('Name')
    is_select_product = fields.Boolean('Is Product')
    product_id = fields.Many2one('product.product', 'Product')
    wizard_id = fields.Many2one('wizard.select.product', string='Wizard ID')
    quantity = fields.Integer('Quantity')
