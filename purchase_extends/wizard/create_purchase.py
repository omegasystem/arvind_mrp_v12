# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _


class WizardCreatePo(models.TransientModel):
    _name = 'wizard.create.po'

    partner_id = fields.Many2one('res.partner', string='Vendor')
    product_ids = fields.One2many('wizard.product.lines', 'wizard_id', string='Products')

    @api.model
    def default_get(self, fields):
        sale_obj = self.env['sale.order']
        result = super(WizardCreatePo, self).default_get(fields)
        if self.env.context.get('active_id') and self.env.context.get('active_model') == 'sale.order':
            sale_order = sale_obj.browse(self.env.context.get('active_id'))
            sale_order_lines = sale_order.order_line.filtered(lambda x: x.is_create_po is True)
            product_ids = [(0, 0,
                            {
                                "product_id": order_line.product_id.id,
                            }
                            ) for order_line in sale_order_lines]
            result['product_ids'] = product_ids
        return result

    def create_po(self):
        print("Self---------", self.env.context)
        sale_obj = self.env['sale.order']
        if self.env.context.get('active_id') and self.env.context.get('active_model') == 'sale.order':
            sale_order = sale_obj.browse(self.env.context.get('active_id'))

            purchase_obj = self.env['purchase.order']
            purchase_obj.create({
                'so_id': sale_order.id,
                'partner_id': self.partner_id.id,
                'order_line': [(0, 0, {'name': product.product_id.name,
                                       'product_id': product.product_id.id,
                                       'product_qty': product.qty,
                                       'product_uom': product.product_id.uom_id.id,
                                       'price_unit': product.product_id.standard_price,
                                       'date_planned': fields.Date.today()
                                       }) for product in self.product_ids]
            })


class WizardProductLines(models.TransientModel):
    _name = 'wizard.product.lines'

    product_id = fields.Many2one('product.product', string='Product')
    qty = fields.Float('Qty')
    wizard_id = fields.Many2one('wizard.create.po', string='Wizard ID')
