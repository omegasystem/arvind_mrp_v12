# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    mo_id = fields.Many2one('mrp.production', string='MO Reference')
    so_id = fields.Many2one('sale.order', string='Purchase Order')

    @api.model
    def create(self, vals):
        so_obj = self.env['sale.order']
        mrp_ovj = self.env['mrp.production']
        if vals.get('origin'):
            mrp_id = mrp_ovj.search([
                ('name', '=', vals.get('origin'))
            ], limit=1)
            vals.update({'mo_id': mrp_id.id})
        return super(PurchaseOrder, self).create(vals)
