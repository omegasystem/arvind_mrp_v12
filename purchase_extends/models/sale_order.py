# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    is_create_po = fields.Boolean('PO')
    is_poll = fields.Boolean('Is Poll')


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def view_sale_order_mrp_plan_product(self):
        action = self.env.ref('mrp_plan.action_mrp_plan_product').read()[0]
        action['domain'] = [('sale_order_id', '=', self.id)]
        action['views'] = [(self.env.ref('mrp_plan.view_mrp_plan_product_tree').id, 'tree')]
        return action
