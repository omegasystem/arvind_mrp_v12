# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
from odoo.exceptions import ValidationError


class StockPicking(models.Model):
    _inherit = "stock.picking"

    vehicle_number = fields.Char('Vehicle Number')
    shipment_date = fields.Date('Shipment Date')
    challan_number = fields.Char('Challan Number')
    challan_date = fields.Date('Challan Date')

    @api.multi
    def write(self, vals):
        return super(StockPicking, self).write(vals)

    @api.one
    @api.constrains('state')
    def check_state(self):
        if self.state == 'done':
            if not self.vehicle_number and not self.shipment_date and not self.challan_number and not self.challan_date:
                raise ValidationError(_('Vehicle Number, Shipment Date,  Challan Number required!'))

    @api.one
    @api.constrains('shipment_date')
    def check_shipment_date(self):
        if self.shipment_date > fields.Date.today():
            raise ValidationError(_('Shipment Date could not be future date!'))
