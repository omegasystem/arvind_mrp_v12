# -*- coding: utf-8 -*-
{
    'name': 'Purchase Extends',
    'category': 'Purchase',
    'sequence': 1,
    'summary': 'Purchase Extends',
    'author': 'Omega System',
    'version': '1.0',
    'description': "",
    'depends': ['purchase', 'stock', 'delivery', 'hr'],
    'data': [
        'security/user_group.xml',
        'wizard/create_purchase_view.xml',
        'wizard/select_product_view.xml',
        'views/stock_view.xml',
        'views/mrp_view.xml',
        'views/sale_order_view.xml',
        'views/purchase_view.xml'
    ],
    'installable': True,
    'application': True,
}
