# -*- coding: utf-8 -*-
import json
from odoo import http, _
from datetime import datetime
from odoo.exceptions import UserError
from odoo.http import request
from odoo.addons.website.controllers.main import Website


class Website(Website):

    @http.route(['/scngmbh/homepage'], type='http', auth="public", website=True)
    def get_homepage(self, **post):
        print("cal------------------------", request)
        page_id = request.env['website.page'].search([
            ('url', '=', '/scngmbh/homepage')
        ])
        block_ids = request.env['website.block'].search([('webpage_url_ids', 'in', page_id.id)])
        print("block_ids-----------------", block_ids)
        values = {
            'block_1': block_ids.block_content
        }
        return request.render("event_extends.scngmbh_homepage_template", values)
