# -*- coding: utf-8 -*-
{
    'name': 'Event Extends',
    'category': 'Event',
    'sequence': 1,
    'summary': 'Event Extends',
    'author': 'Omega System',
    'version': '1.0',
    'description': "",
    'depends': ['event', 'website_event', 'website_event_sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/event_view.xml',
        'views/homepage.xml',
        'views/website_block_view.xml',
        'data/menu_data.xml'
    ],
    'installable': True,
    'application': True,
}
