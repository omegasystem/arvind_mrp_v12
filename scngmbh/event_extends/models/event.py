# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class EventEvent(models.Model):
    _inherit = "event.event"

    website_page_url = fields.Char('Website Url')
    certification = fields.Selection([
        ('yes', 'Yes, of course'),
        ('no', 'No')
    ], string='Certification')
    event_catering_ids = fields.Many2many('event.catering', string='Catering')
    event_methods_id = fields.Many2one('event.methods', string='Event Methods')
    seminars_types = fields.Selection([
        ('public', 'Public'),
        ('in_house', 'In House')
    ], string='Seminars Types')
    implementation_guarantee = fields.Text('Implementation guarantee')
    seminar_documents = fields.Text('Seminar documents')
    barrier_access = fields.Text('Barrier-free access')
    event_support = fields.Text('Support')
    seminar_category = fields.Selection([
        ('public', 'Public Seminar'),
        ('company', 'Company Seminar'),
        ('online', 'Online Webinar')
    ], string='Seminar Category')


class EventCatering(models.Model):
    _name = "event.catering"

    name = fields.Char('Catering Name')


class EventMethods(models.Model):
    _name = "event.methods"

    name = fields.Char('Name')



