# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class WebsiteBlock(models.Model):
    _name = "website.block"
    _rec_name = "block_name"

    block_name = fields.Char('Block Name')
    block_content = fields.Text('Block Content')
    webpage_url_ids = fields.Many2many('website.page', string='Webpage')


