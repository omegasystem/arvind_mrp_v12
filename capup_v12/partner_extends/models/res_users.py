# -*- coding: utf-8 -*-
from odoo import api, models, fields


class ResUsers(models.Model):
    _inherit = "res.users"

    enable_payment = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')], string='Enable Payment',
        default='no')



