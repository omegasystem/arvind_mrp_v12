# -*- coding: utf-8 -*-
{
    'name': 'Partner Extends',
    'category': 'Sales',
    'sequence': 1,
    'summary': 'Partner Extends',
    'author': 'Omega System',
    'version': '1.0',
    'description': "",
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'data/res_groups.xml',
        'views/res_partner_view.xml',
    ],
    'installable': True,
    'application': True,
}
