#-*- coding:utf-8 -*-
from odoo import api, models, _
from datetime import datetime


class BankProcessReport(models.AbstractModel):
    _name = 'report.capup_extends.report_bank_process_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, lines):
        wizard_record = data.get('data')

        sheet = workbook.add_worksheet("")
        # format1 = workbook.add_format({'font_size': 12, 'bg_color': '#D3D3D3'})
        # format2 = workbook.add_format({'font_size': 12, 'bold': True, 'bg_color': '#D3D3D3'})
        # format3 = workbook.add_format({'font_size': 10})
        # format5 = workbook.add_format({'font_size': 10, 'bold': True})
        # sheet.merge_range('A1:D1', "Mountain Shade Australia", format2)
        # sheet.merge_range('A2:B2', "From Date", format3)
        # sheet.merge_range('C2:E2', data.get('data')['start_date'], format3)
        # sheet.merge_range('A3:B3', "To Date", format3)
        # sheet.merge_range('C3:E3', data.get('data')['end_date'], format3)

        # sheet.set_column(1, 1, 20)
        sheet.set_column(14, 0, 20)
        row = 0
        column = 0
        sheet.write(row, column, "Client_Code")
        sheet.write(row, column + 1, "Product_Code")
        sheet.write(row, column + 2, "Payment_Type")
        sheet.write(row, column + 3, "Payment_Ref_No.")
        sheet.write(row, column + 4, "Payment_Date")
        sheet.write(row, column + 5, "Instrument Date")
        sheet.write(row, column + 6, "Dr_Ac_No")
        sheet.write(row, column + 7, "Amount")
        sheet.write(row, column + 8, "Bank_Code_Indicator")
        sheet.write(row, column + 9, "Beneficiary_Code")
        sheet.write(row, column + 10, "Beneficiary_Name")
        sheet.write(row, column + 11, "Beneficiary_Bank")
        sheet.write(row, column + 11, "Beneficiary_Branch / IFSC Code")
        sheet.write(row, column + 12, "Beneficiary_Acc_No")
        sheet.write(row, column + 13, "Location")
        sheet.write(row, column + 14, "Print_Location")
        sheet.write(row, column + 15, "Instrument_Number")
        sheet.write(row, column + 16, "Ben_Add1")
        sheet.write(row, column + 17, "Ben_Add2")
        sheet.write(row, column + 18, "Ben_Add3")
        sheet.write(row, column + 19, "Ben_Add4")
        sheet.write(row, column + 20, "Beneficiary_Email")
        sheet.write(row, column + 21, "Beneficiary_Mobile")
        sheet.write(row, column + 22, "Debit_Narration")
        sheet.write(row, column + 23, "Credit_Narration")
        sheet.write(row, column + 24, "Payment Details 1")
        sheet.write(row, column + 25, "Payment Details 2")
        sheet.write(row, column + 26, "Enrichment_1")
        sheet.write(row, column + 27, "Enrichment_2")
        sheet.write(row, column + 28, "Enrichment_3")
        sheet.write(row, column + 29, "Enrichment_4")


        # sale_obj = self.env['sale.order']
        # invoice_obj = self.env['account.invoice']
        # picking_obj = self.env['stock.picking']
        # total_commissionable_amount = 0.0
        # commission_payble = 0.0
        # row = 5
        # for invoice_record in data.get('record'):
        #     order = invoice_obj.browse(invoice_record)
        #     shipping_amount = 0.0
        #     product_amount = 0.0
        #     shipping_tax = 0.0
        #
        #     sale_id = sale_obj.search([
        #         ('name', '=', order.origin)
        #     ], limit=1)
        #     picking_id = picking_obj.search([
        #         ('origin', '=', order.origin),
        #         ('state', '=', 'done')
        #     ])
        #     if order.partner_id.customer_type in ['prospect', 'customer']:
        #         commisstion_rate = 5
        #     elif order.partner_id.customer_type == 'reseller':
        #         commisstion_rate = 3
        #     if sale_id.opportunity_id:
        #         sheet.write(row, 0, sale_id.opportunity_id[0].name, format1)
        #
        #         # sheet.write('A6', sale_id.opportunity_id[0].name, format1)
        #     if sale_id:
        #         sheet.write(row, 1, sale_id.name, format1)
        #         # sheet.write('B6', sale_id.name)
        #     else:
        #         sheet.write(row, 1, sale_id.name, format1)
        #         # sheet.write('B6', order.origin)
        #     sheet.write(row, 2, str(order.date_invoice))
        #     sheet.write(row, 3, str(order.date_due))
        #     sheet.write(row, 4, str(order.do_dispatch_date))
        #     sheet.write(row, 5, order.user_id.name)
        #     sheet.write(row, 6, order.team_id.name)
        #
        #     for order_line in order.invoice_line_ids:
        #         if order_line.product_id.default_code in ['PC0001', 'PC0002', 'PICKWARE']:
        #             shipping_amount = order_line.price_total
        #             shipping_tax = \
        #                 order_line.invoice_line_tax_ids.compute_all(order_line.price_unit, order.currency_id,
        #                                                             order_line.quantity, order_line.product_id,
        #                                                             order.partner_id)['taxes']
        #
        #         else:
        #             product_amount += order_line.price_total
        #
        #
        #     order_total = order.amount_total - shipping_amount
        #     total_commissionable_amount = order_total - order.amount_tax
        #     commission_payble = total_commissionable_amount * commisstion_rate / 100
        #
        #     sheet.write(row, 7, order.amount_total)
        #     sheet.write(row, 8, shipping_amount)
        #     sheet.write(row, 9, order_total)
        #     sheet.write(row, 10, order.amount_tax)

            # # if shipping_tax:
            # #     if shipping_tax != 0.0 and shipping_tax:
            # #         sheet.write(row, 10, shipping_tax[0].get('amount'))
            # #         # sheet.write('K6', shipping_tax[0].get('amount'))
            # #     else:
            # #         sheet.write(row, 10, shipping_tax)
            # #         # sheet.write('K6', shipping_tax)
            # # else:
            # #     sheet.write(row, 10, 0)
            #     # sheet.write('K6', '0')
            #
            # if picking_id:
            #     sheet.write(row, 11, total_commissionable_amount)
            #     sheet.write(row, 12, commisstion_rate)
            #     sheet.write(row, 13, commission_payble)
            #     # sheet.write('L6', total_commissionable_amount)
            #     # sheet.write('M6', commisstion_rate)
            #     # sheet.write('N6', commission_payble)
            # row = row + 1

