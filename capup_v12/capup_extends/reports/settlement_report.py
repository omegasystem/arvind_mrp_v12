#-*- coding:utf-8 -*-
from odoo import api, models, _
from datetime import datetime


class SettlementReport(models.AbstractModel):
    _name = 'report.capup_extends.settlement_report_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        settlement_list = []
        for record in data.get('settlement_ids'):
            settlement = self.env['client.settlement'].browse(int(record))
            settlement_list.append(settlement)
        return {
            'doc_ids': data.get('settlement_ids'),
            'doc_model': 'crm.lead',
            'docs': docids,
            'data': data,
            'settlement_list': settlement_list
        }


class ClientTotalSettlementReport(models.AbstractModel):
    _name = 'report.capup_extends.client_total_settlement_report_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        settlement_data = []
        domain = [
            ('settlement_date', '>=',  data.get('form')['start_date']),
            ('settlement_date', '<=', data.get('form')['end_date'])
        ]
        for record in data.get('all_clients'):
            client_dict = {}
            client = self.env['res.partner'].browse(record)
            domain += [('client_id', '=', client.id)]
            settlements = self.env['client.settlement'].search(domain)
            settlement_amount = 0.0
            for settlement in settlements:
                settlement_amount += settlement.settlement_amount
            client_dict.update({'client_id': client, 'settlement_amount': settlement_amount})
            settlement_data.append(client_dict)
        return {
            'doc_ids': data.get('settlement_ids'),
            'doc_model': 'client.settlement',
            'docs': docids,
            'data': data,
            'settlement_data': settlement_data
        }


class ClientTotalPendingReport(models.AbstractModel):
    _name = 'report.capup_extends.client_total_pending_report_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        settlement_data = []
        domain = [
            ('settlement_date', '>=',  data.get('form')['start_date']),
            ('settlement_date', '<=', data.get('form')['end_date'])
        ]
        for record in data.get('all_clients'):
            client_dict = {}
            client = self.env['res.partner'].browse(record)
            domain += [('client_id', '=', client.id)]
            settlements = self.env['client.settlement'].search(domain)
            settlement_amount = 0.0
            pending_amount_total = 0.0
            for settlement in settlements:
                settlement_amount += settlement.settlement_amount
                pending_amount_total += settlement.receivable_amount - settlement.received_amount
            client_dict.update({'client_id': client, 'settlement_amount': settlement_amount, 'pending_amount': pending_amount_total})
            settlement_data.append(client_dict)
        return {
            'doc_ids': data.get('settlement_ids'),
            'doc_model': 'client.settlement',
            'docs': docids,
            'data': data,
            'settlement_data': settlement_data
        }