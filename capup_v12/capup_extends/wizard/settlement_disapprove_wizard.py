# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class SettlementDisapprove(models.TransientModel):
    _name = 'settlement.disapprove'

    desc = fields.Text('Reason')

    @api.multi
    def submit_desc(self):
        print("seld----------", self.env.context)
        active_id = self.env['client.settlement'].browse(self.env.context.get('active_ids'))
        active_id.write({
            'status_reason': self.desc,
            'state': self.env.context.get('status')
        })

