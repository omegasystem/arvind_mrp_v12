# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class FollowupWizard(models.TransientModel):
    _name = 'followup.wizard'

    next_followup_date = fields.Date('Next Followup Date')
    desc = fields.Text('Reason')
    received_amount = fields.Float('Received Amount')

    @api.multi
    def submit(self):
        active_id = self.env['crm.lead'].browse(self.env.context.get('active_ids'))
        active_id.running_followup_id.end_timedate = fields.Datetime.now()
        active_id.running_followup_id.desc = self.desc
        active_id.write({
            'next_followup_date': self.next_followup_date,
            'is_running': False,
            'running_followup_id': ''
        })

