# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CreateUser(models.TransientModel):
    _name = 'create.user'

    login = fields.Char('Email')
    password = fields.Char('Password')

    @api.multi
    def create_user(self):
        user_obj = self.env['res.users']
        exit_id = user_obj.search([
            ('login', '=', self.login)
        ])
        if exit_id:
            raise ValidationError(_(
                'This login already exist in system!'))
        user_id = user_obj.sudo().create({
            'company_id': self.env.ref("base.main_company").id,
            'name': self.login,
            'login': self.login,
            'email': self.login,
            # 'groups_id': [(6, 0, [self.ref('sales_team.group_sale_manager')])]
        })
        if self.env.user.partner_id and self.env.user.partner_id.parent_id:
            parent_id = self.env['res.users'].search([
                ('partner_id', '=', self.env.user.partner_id.parent_id.id)
            ])
            user_id.partner_id.write({'created_by': self.env.user.id, 'supplier': True, 'client_id': parent_id.id})
            # self.env['scheduled.mail'].create({
            #     'vendor_id': user_id.partner_id.id,
            #     'scheduled_type': 'draft',
            #     'sent_to': 'vendor'
            # })


