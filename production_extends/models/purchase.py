# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    plm_id = fields.Many2one('plm.requirement', string='PLM')

    @api.model
    def create(self, vals):
        if self.env.context.get('active_model') == 'plm.requirement':
            vals.update({'plm_id':  self.env.context.get('active_id')})
        return super(PurchaseOrder, self).create(vals)

