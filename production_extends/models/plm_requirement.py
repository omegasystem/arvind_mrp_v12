# -*- encoding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp


class PlmRequirement(models.Model):
    _name = 'plm.requirement'

    product_id = fields.Many2one('product.template', string='Product')
    bom_line = fields.One2many('plm.product.bom.line', 'order_id', string='Material Requirements')

    def check_bom(self):
        return {
            'name': 'BOM',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'res_model': 'mrp.bom',
            # 'context': {'default_product_tmpl_id': 54, 'search_default_product_tmpl_id': 54}
            'domain': [('product_tmpl_id', '=', self.product_id.id)]
        }

    def fillup_bom(self):
        if self.product_id:
            bom_ids = self.env['mrp.bom'].search([
                ('product_tmpl_id', '=', self.product_id.id)
            ], limit=1)
            for bom in bom_ids:
                bom_lines = [(0, 0, {'product_id': line.product_id.id, 'product_qty': line.product_qty}) for line in bom.bom_line_ids]
            self.bom_line = bom_lines

    def create_purchase(self):
        return {
            'name': 'Purchase Order',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'target': 'new',
            'res_model': 'purchase.order',
            # 'domain': [('product_tmpl_id', '=', self.product_id.id)]
        }

    @api.multi
    def action_open_purchase_order(self):
        action = self.env.ref('purchase.purchase_rfq').read()[0]
        action['domain'] = [('plm_id', 'in', [self.id])]
        action['views'] = [(self.env.ref('purchase.purchase_order_tree').id, 'tree')]
        return action


class PlmProductBomLine(models.Model):
    _name = 'plm.product.bom.line'

    product_qty = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'), required=False)
    product_id = fields.Many2one('product.product', string='Product', required=False)

    order_id = fields.Many2one('plm.requirement', string='Order Reference', required=False)


