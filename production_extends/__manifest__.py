# -*- coding: utf-8 -*-
{
    'name': 'Production Extends',
    'category': 'mrp',
    'sequence': 1,
    'summary': 'Production Extends',
    'author': 'Omega System',
    'version': '1.0',
    'description': "",
    'depends': ['purchase', 'stock', 'delivery', 'mrp'],
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/stock_view.xml',
        # 'views/menu.xml',
        # 'views/plm_requirement_view.xml'
    ],
    'installable': True,
    'application': True,
}
